<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passport extends Model
{
    protected $fillable = ['family_name', 'given_names', 'father_name', 'grandfather_name', 'birth'];
}
