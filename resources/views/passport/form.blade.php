<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Family Name</label>
    <div class="col-sm-10">
       {!! Form::text('family_name', null, ['class' => 'form-control', 'placeholder' => '']) !!}
    </div>
</div>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Given Names:</label>
    <div class="col-sm-10">
       {!! Form::text('given_names', null, ['class' => 'form-control', 'placeholder' => '']) !!}
    </div>
</div>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Father's Full Name:</label>
    <div class="col-sm-10">
       {!! Form::text('father_name', null, ['class' => 'form-control', 'placeholder' => '']) !!}
    </div>
</div>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Grandfather's Full Name:</label>
    <div class="col-sm-10">
       {!! Form::text('grandfather_name', null, ['class' => 'form-control', 'placeholder' => '']) !!}
    </div>
</div>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Date of Birth (Gregorian):</label>
    <div class="col-sm-10">
       {!! Form::date('birth', null, ['class' => 'form-control', 'placeholder' => '']) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
       <button type="submit" class="btn btn-default">{{ $submiteText }}</button>
    </div>
</div>
