@extends('master')

@section('content')

	{!! Form::open(['route' => 'passport.store' , 'class' => 'form-horizontal']) !!}	
		@include('passport.form',['submiteText' => 'Create'])
	{!! Form::close() !!}
@stop