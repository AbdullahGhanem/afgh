@extends('master')

@section('content')
	@foreach(App\Passport::all() as $passport)
		<div class="jumbotron">
			<h3>family_name : {{ $passport->family_name }}</h3>
			<h5>given_names : {{ $passport->given_names }}</h5>
			<h5>father_name : {{ $passport->father_name }}</h5>
			<h5>grandfather_name : {{ $passport->grandfather_name }}</h5>


			<h5>
				<a class="btn btn-small btn-primary pull-right" href="{{ route('passport.show', $passport->id) }}" role="button">download pdf</a>
			</h5>
		</div>
	@endforeach
@stop